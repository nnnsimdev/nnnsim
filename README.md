# nnnsim
NS-3 Module for the mobility aware, full network naming network architecture, 3N. Includes support for Information Centric Networks.

For more information regarding compilation instructions, please refer to the documents in the doc/ folder.

# Publications

Information about nnnsim and the research for which it was developed can be found in the following papers:

* López, J., Nguyen, Q. N., Wen, Z., Yu, K., & Sato, T. (2019). Using linguistic properties of place specification for network naming to improve mobility performance. Sensors (Switzerland), 19(13), [2888]. https://doi.org/10.3390/s19132888
* López, J., & Sato, T. (2017). Seamless mobility in ICN for mobile consumers with mobile producers. IEICE Transactions on Communications, E100B(10), 1827-1836. https://doi.org/10.1587/transcom.2016EBP3435
* López, J. E., Arifuzzaman, M., Zhu, L., Wen, Z., & Sato, T. (2016). Seamless mobility in data aware networking. ： Proceedings of the 2015 ITU Kaleidoscope: Trust in the Information Society, K-2015 - Academic Conference [7383640] Institute of Electrical and Electronics Engineers Inc.. https://doi.org/10.1109/Kaleidoscope.2015.7383640
