/* -*- Mode:C++; c-file-style:"gnu"; -*- */
/*
 * nnn-test-naming.h
 *  Basic tests for 3N naming
 *
 * Copyright (c) 2020 Jairo Eduardo Lopez
 * Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 *
 */

// ns3 modules
#include "ns3/test.h"
#include "ns3/config.h"
#include "ns3/core-module.h"

// nnnSIM modules 
#include "ns3/nnnsim-module.h"

using namespace ns3;
using namespace boost;
using namespace std;

namespace ns3
{
  namespace nnn
  {
      class Generic3NTest : public virtual TestCase
      {
        public:
          // Create the expected 3N names
	  Ptr<const NNNAddress> name00 = Create <NNNAddress> ("");
          Ptr<const NNNAddress> name0 = Create <NNNAddress> ("2.0.1");
          Ptr<const NNNAddress> name1 = Create <NNNAddress> ("2.0.0");
          Ptr<const NNNAddress> name2 = Create <NNNAddress> ("2.0.2");
          Ptr<const NNNAddress> name3 = Create <NNNAddress> ("1.0");
          Ptr<const NNNAddress> name4 = Create <NNNAddress> ("a.0");
          Ptr<const NNNAddress> name5 = Create <NNNAddress> ("1.0.1.1");
          Ptr<const NNNAddress> name6 = Create <NNNAddress> ("5.0.0.1");
          Ptr<const NNNAddress> name7 = Create <NNNAddress> ("2.0.1.1");
          Ptr<const NNNAddress> name8 = Create <NNNAddress> ("a.1");
          Ptr<const NNNAddress> name9 = Create <NNNAddress> ("b");
          Ptr<const NNNAddress> name10 = Create <NNNAddress> ("a.1.0.1");
          Ptr<const NNNAddress> name11 = Create <NNNAddress> ("d");
          Ptr<const NNNAddress> parent00 = Create<NNNAddress> ("2");
          Ptr<const NNNAddress> parent01 = Create<NNNAddress> ("1");
          Ptr<const NNNAddress> parent02 = Create<NNNAddress> ("2.0");
          Ptr<const NNNAddress> parent03 = Create<NNNAddress> ("2.1");
          Ptr<const NNNAddress> subsector00 = Create<NNNAddress> ("2.0.1.0");
          Ptr<const NNNAddress> subsector01 = Create<NNNAddress> ("2.0.1.2");

          Generic3NTest ()
            : TestCase ("Generic3N")
            {
            }
        
          virtual void
          DoRun (void)
            {
            }

          // Symmetric label comparison test
          void
          compareNames (Ptr<const NNNAddress> name1,
                        Ptr<const NNNAddress> name2, int result,
                        std::string error_str)
            {
              int res = 0;
              res = name1->compare (*name2);
              NS_TEST_EXPECT_MSG_EQ (res, result, "" << *name1 << error_str <<
                                     *name2 << " using compare, got " << res);

              res = name1->compareLabels (*name2);
              NS_TEST_EXPECT_MSG_EQ (res, result, "" << *name1 << error_str <<
                                     *name2 << " using compareLabels, got " <<
                                     res);
            }

          // Asymmetric closest sector comparison of labels
          void
          compareNameHops (Ptr<const NNNAddress> name1,
                           Ptr<const NNNAddress> name2, int result,
                           std::string error_str)
            {
              int res = name1->compareName (*name2);
              NS_TEST_EXPECT_MSG_EQ (res, result, "" << *name1 << error_str <<
                                   *name2 << " using compareName, got " << res);
            }

    };

    class NamingEquivalenceTest : public Generic3NTest
    {
      public:
        NamingEquivalenceTest ()
          : TestCase ("NamingEquivalence")
          {
          }

        virtual void
        DoRun (void)
          {
            std::string str_eq = " should be equal to ";
            compareNames (name00, name00, 0, str_eq);
            compareNames (name0, name0, 0, str_eq);
            compareNames (name3, name3, 0, str_eq);
            compareNames (name6, name6, 0, str_eq);
            // ClosestSector based comparison
            compareNameHops (name0, name0, 0, str_eq);
            compareNameHops (name3, name3, 0, str_eq);
            compareNameHops (name6, name6, 0, str_eq);
          }
    };

    class NamingInferiorTest : public Generic3NTest
    {
      public:
        NamingInferiorTest ()
          : TestCase ("NamingInferior")
          {
          }

        virtual void
        DoRun (void)
          {
            std::string str_less = " should be inferior to ";
            compareNames (name00, name0, -1, str_less);
            compareNames (name0, name2, -1, str_less);
            compareNames (name0, name4, -1, str_less);
            compareNames (name0, name6, -1, str_less);
            compareNames (name1, name0, -1, str_less);
            compareNames (name3, name0, -1, str_less);
            compareNames (name5, name0, -1, str_less);

            // ClosestSector based comparison
            compareNameHops (name0, name2, -1, str_less);
            compareNameHops (name0, name6, -1, str_less);
          }
    };

    class NamingSuperiorTest : public Generic3NTest
    {
      public:
        NamingSuperiorTest ()
          : TestCase ("NamingSuperior")
          {
          }

        virtual void
        DoRun (void)
          {
            std::string str_more = " should be superior to ";
            compareNames (name0, name00, 1, str_more);
            compareNames (name0, name1, 1, str_more);
            compareNames (name0, name3, 1, str_more);
            compareNames (name0, name5, 1, str_more);
            compareNames (name2, name0, 1, str_more);
            compareNames (name4, name0, 1, str_more);
            compareNames (name6, name0, 1, str_more);

            // ClosestSector based comparison
            compareNameHops (name0, name1, 1, str_more);
            compareNameHops (name0, name3, 1, str_more);
            /* Note that in the following case, (2.0.1) vs (1.0.1.1), (1)
             * becomes the closest sector making the distance of these two
             * names the same. In the following comparison, the 2 in (2.0.1)
             * is larger than 1 in (1.0.1.1), giving this unexpected result
             * that (2.0.1) is larger than (1.0.1.1)
             */
            compareNameHops (name0, name5, 1, str_more);
            compareNameHops (name0, name4, 1, str_more);
          }
    };

    class NamingDistanceTest : public Generic3NTest
    {
      public:
        NamingDistanceTest ()
          : TestCase ("NamingDistance")
          {
          }

        virtual void
        DoRun (void)
          {
            // Empty name
            compareDistance (name00, name0, 3);
            compareDistance (name0, name00, 3);

            compareDistance (name00, name10, 4);
            compareDistance (name10, name00, 4);

            // Identity
            compareDistance (name0, name0, 0);
            compareDistance (name6, name6, 0);

            // Tree distance and reflexivity

            // Same branch
            compareDistance (name0, name7, 1);
            compareDistance (name7, name0, 1);

            // Different trees, next to each other 
            compareDistance (name9, name11, 1);
            compareDistance (name11, name9, 1);

            // Same branch, 2 hops away
            compareDistance (name8, name10, 2);
            compareDistance (name10, name8, 2);

            // Different branch, common node one hop away
            compareDistance (name0, name1, 2);
            compareDistance (name1, name0, 2);

            compareDistance (name0, name2, 2);
            compareDistance (name2, name0, 2);

            // Different branch, common node two hops away
            compareDistance (name1, name7, 3);
            compareDistance (name7, name1, 3);

            // Different tree
            compareDistance (name0, name3, 4);
            compareDistance (name3, name0, 4);

            // Different tree
            compareDistance (name0, name6, 6);
            compareDistance (name6, name0, 6);
          }

        // Distance test
        void
        compareDistance (Ptr<const NNNAddress> name1,
                         Ptr<const NNNAddress> name2, size_t expected)
          {
            size_t result = name1->distance (*name2);
            NS_TEST_EXPECT_MSG_EQ (result, expected, "Got " << result <<
                                   " for distance from " << *name1 <<
                                   " to " << *name2 << ", expected " <<
                                   expected);
          }
    };

    class NamingSubSectorTest : public Generic3NTest
    {
      public:
        NamingSubSectorTest ()
          : TestCase ("NamingSubSector")
          {
          }

        // Subsector property test
        void
        compareSubSector (Ptr<const NNNAddress> name1,
                          Ptr<const NNNAddress> name2, bool expected)
          {
            bool result = name1->isSubSectorSet (name2);

            std::string conjuntive = expected ? " to be " : " not to be ";

            NS_TEST_EXPECT_MSG_EQ (result, expected, "Expected " << *name1 <<
                                   conjuntive << "subsector of " << *name2);
          }

        void
        DoRun (void)
          {
            // Empty 3N name asymmetry
            //   When comparing with an empty 3N name, the result is always
            //   false
            compareSubSector (name0, name00, false);
            compareSubSector (name00, name0, false);

            // Asymmetry
            compareSubSector (name0, name0, false);

            // Same branch parent
            compareSubSector (name0, parent00, true);
            compareSubSector (parent00, name0, false);

            // Different root
            compareSubSector (name0, parent01, false);
            compareSubSector (parent01, name0, false);

            // Same branch, direct parent
            compareSubSector (name0, parent02, true);
            compareSubSector (parent02, name0, false);

            // Different branch
            compareSubSector (name0, parent03, false);
            compareSubSector (parent03, name0, false);

            // Children
            compareSubSector (name0, subsector00, false);
            compareSubSector (subsector00, name0, true);
            compareSubSector (name0, subsector01, false);
            compareSubSector (subsector01, name0, true);
          }
      };

    class NamingSameSectorTest : public Generic3NTest
    {
      public:
        NamingSameSectorTest ()
          : TestCase ("NamingSameSector")
          {
          }

        void
        compareSameSector (Ptr<const NNNAddress> name1,
                           Ptr<const NNNAddress> name2, bool expected)
          {
              bool result = name1->isSameSector (*name2);

              std::string conjuntive = expected ? " to be " : " not to be ";

              NS_TEST_EXPECT_MSG_EQ (result, expected, "Expected " << *name1 <<
                                     conjuntive << "in same sector as " <<
                                     *name2);
          }

        void
        DoRun (void)
          {
            // Empty 3N name asymmetry
            //   When comparing with an empty 3N name, the result is always
            //   false
            compareSameSector (name0, name00, false);
            compareSameSector (name00, name0, false);

            // Symmetry
            compareSameSector (name0, name0, true);

            compareSameSector (name0, name1, true);
            compareSameSector (name1, name0, true);

            compareSameSector (name0, parent02, false);
            compareSameSector (parent02, name0, false);

            compareSameSector (name0, subsector00, false);
            compareSameSector (subsector00, name0, false);
          }
    };

    class NamingParentSectorTest : public Generic3NTest
    {
      public:
        NamingParentSectorTest ()
          : TestCase ("NamingParentSector")
          {
          }

        void
        compareParentSector (Ptr<const NNNAddress> name1,
                             Ptr<const NNNAddress> name2, bool expected)
          {
              bool result = name1->isParentSectorSet (name2);

              std::string conjuntive = expected ? " to be " : " not to be ";

              NS_TEST_EXPECT_MSG_EQ (result, expected, "Expected " << *name1 <<
                                     conjuntive << "parent sector of " <<
                                     *name2);
          }

        void
        DoRun (void)
          {
            // Empty 3N name asymmetry
            //   When comparing with an empty 3N name, the result is always
            //   false
            compareParentSector (name0, name00, false);
            compareParentSector (name00, name0, false);

            // Symmetry
            compareParentSector (name0, name0, false);

            compareParentSector (name0, name1, false);
            compareParentSector (name1, name0, false);

            compareParentSector (name0, parent00, false);
            compareParentSector (parent00, name0, true);

            compareParentSector (name0, parent02, false);
            compareParentSector (parent02, name0, true);

            compareParentSector (name0, subsector00, true);
            compareParentSector (subsector00, name0, false);

            compareParentSector (name0, name11, false);
            compareParentSector (name11, name0, false);
          }
    };

    class NamingDirectSubSectorTest : public Generic3NTest
    {
      public:
        NamingDirectSubSectorTest ()
          : TestCase ("NamingDirectSubSector")
          {
          }

        void
        compareDirectSubSector (Ptr<const NNNAddress> name1,
                             Ptr<const NNNAddress> name2, bool expected)
          {
              bool result = name1->isDirectSubSector (*name2);

              std::string conjuntive = expected ? " to be " : " not to be ";

              NS_TEST_EXPECT_MSG_EQ (result, expected, "Expected " << *name1 <<
                                     conjuntive << "direct subsector of " <<
                                     *name2);
          }

        void
        DoRun (void)
          {
            // Empty 3N name asymmetry
            //   When comparing with an empty 3N name, the result is always
            //   false
            compareDirectSubSector (name0, name00, false);
            compareDirectSubSector (name00, name0, false);

            // Asymmetry
            compareDirectSubSector (name0, name0, false);

            // Same branch parent
            compareDirectSubSector (name0, parent00, false);
            compareDirectSubSector (parent00, name0, false);

            // Different root
            compareDirectSubSector (name0, parent01, false);
            compareDirectSubSector (parent01, name0, false);

            // Same branch, direct parent
            compareDirectSubSector (name0, parent02, true);
            compareDirectSubSector (parent02, name0, false);

            // Different branch
            compareDirectSubSector (name0, parent03, false);
            compareDirectSubSector (parent03, name0, false);

            // Children
            compareDirectSubSector (name0, subsector00, false);
            compareDirectSubSector (subsector00, name0, true);
            compareDirectSubSector (name0, subsector01, false);
            compareDirectSubSector (subsector01, name0, true);
          }
    };

    class NamingClosestSectorTest : public Generic3NTest
    {
      public:
        NamingClosestSectorTest ()
          : TestCase ("NamingClosestSector")
          {
          }

        void
        compareClosestSector (Ptr<const NNNAddress> name1,
                              Ptr<const NNNAddress> name2,
                              Ptr<const NNNAddress> expected)
          {
            NNNAddress nearest = name1->getClosestSector (*name2);

            int result = nearest.compare (*expected);

            NS_TEST_EXPECT_MSG_EQ (result, 0,
                                   "Expected closest sector between " <<
                                   *name1 << " and " << *name2 << " to be " <<
                                   *expected << ", obtained " << nearest);
          }

        void
        DoRun (void)
          {
            // Empty 3N name always returns empty
            compareClosestSector (name00, name0, name00);
            compareClosestSector (name0, name00, name00);

            // Identity
            compareClosestSector (name0, name0, name0);

            // Same root
            compareClosestSector (name0, parent00, parent00);
            compareClosestSector (name0, parent00, parent00);

            // Same lower root
            compareClosestSector (subsector00, name2, parent02);
            compareClosestSector (name2, subsector00, parent02);

            compareClosestSector (subsector00, subsector01, name0);
            compareClosestSector (subsector00, subsector01, name0);

            // Different root
            //  In these cases, return the root of the name being compared
            compareClosestSector (name5, name7, parent00);
            compareClosestSector (name7, name5, parent01);
          }
    };
  }
}
