/* -*- Mode:C++; c-file-style:"gnu" -*- */
/*
 * Copyright (c) 2015 Waseda University, Sato Laboratory
 *
 *   This file is part of nnnsim.
 *
 *  nnn-pit-entry-incoming-face.cc is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-pit-entry-incoming-face.cc is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-pit-entry-incoming-face.cc. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#include "nnn-pit-entry-incoming-face.h"

#include "ns3/nnn-address.h"

namespace ns3
{
  namespace nnn
  {
    namespace pit
    {
      IncomingFace::IncomingFace (Ptr<Face> face)
      : m_face (face)
      , m_addrs (Create<NNNAddrAggregator> ())
      , m_arrivalTime (Simulator::Now ())
      , m_raw_icn (false)
      , m_saw_null (true)
      // , m_nonce (nonce)
      {
      }

      IncomingFace::IncomingFace (Ptr<Face> face, Ptr<const NNNAddress> addr)
      : m_face (face)
      , m_addrs (Create<NNNAddrAggregator> ())
      , m_arrivalTime (Simulator::Now ())
      , m_raw_icn (false)
      , m_saw_null (false)
      {
	m_addrs->AddDestination(addr);
      }

      IncomingFace::IncomingFace (Ptr<Face> face, bool raw_icn)
      : m_face (face)
      , m_addrs (Create<NNNAddrAggregator> ())
      , m_arrivalTime (Simulator::Now ())
      , m_raw_icn (raw_icn)
      , m_saw_null (false)
      {
      }

      IncomingFace::IncomingFace (Ptr<Face> face, Address addr)
      : m_face (face)
      , m_addrs (Create<NNNAddrAggregator> ())
      , m_arrivalTime (Simulator::Now ())
      , m_raw_icn (false)
      , m_saw_null (false)
      {
	AddPoADestination (addr);
      }

      IncomingFace::IncomingFace (Ptr<Face> face, Address addr, bool raw_icn, bool saw_null)
      : m_face (face)
      , m_addrs (Create<NNNAddrAggregator> ())
      , m_arrivalTime (Simulator::Now ())
      , m_raw_icn (false)
      , m_saw_null (false)
      {
	AddPoADestination (addr);
	EnableRawICNonPoA(addr, raw_icn);
	EnableSawNULLonPoA(addr, saw_null);
      }

      IncomingFace::IncomingFace ()
      : m_face (0)
      , m_addrs (Create<NNNAddrAggregator> ())
      , m_arrivalTime (0)
      , m_raw_icn (false)
      , m_saw_null (false)
      {
      }

      void
      IncomingFace::AddDestination(Ptr<const NNNAddress> addr)
      {
	m_addrs->AddDestination(addr);
      }

      void
      IncomingFace::RemoveDestination(Ptr<const NNNAddress> addr)
      {
	m_addrs->RemoveDestination(addr);
      }

      void
      IncomingFace::AddPoADestination (Address addr)
      {
	m_poa_addrs[addr] = PoAInInfo ();
      }

      std::map<Address,PoAInInfo>
      IncomingFace::GetPoADestinations () const
      {
	return m_poa_addrs;
      }

      void
      IncomingFace::RemovePoADestination (Address addr)
      {
	m_poa_addrs.erase(addr);
      }

      bool
      IncomingFace::NoAddresses()
      {
	return m_addrs->isEmpty();
      }

      bool
      IncomingFace::DestinationExists (Ptr<const NNNAddress> addr)
      {
	return m_addrs->DestinationExists(addr);
      }

      void
      IncomingFace::EnableRawICN (bool enable)
      {
	m_raw_icn = enable;
      }

      bool
      IncomingFace::SawRawICN () const
      {
	return m_raw_icn;
      }

      void
      IncomingFace::EnableSawNULL (bool enable)
      {
	m_saw_null = enable;
      }

      bool
      IncomingFace::SawNULL () const
      {
	return m_saw_null;
      }

      void
      IncomingFace::EnableRawICNonPoA (Address poa, bool enable)
      {
	std::map<Address,PoAInInfo>::iterator it = m_poa_addrs.find (poa);

	if (it != m_poa_addrs.end ())
	  {
	    (it->second).m_raw_icn = enable;
	  }
	else
	  {
	    PoAInInfo tmp = PoAInInfo ();
	    tmp.m_raw_icn = enable;
	    m_poa_addrs[poa] = tmp;
	  }
      }

      bool
      IncomingFace::SawRawICNonPoA (Address poa)
      {
	std::map<Address,PoAInInfo>::iterator it = m_poa_addrs.find (poa);

	if (it != m_poa_addrs.end ())
	  {
	    return (it->second).m_raw_icn;
	  }
	else
	  return false;
      }

      void
      IncomingFace::EnableSawNULLonPoA (Address poa, bool enable)
      {

	std::map<Address,PoAInInfo>::iterator it = m_poa_addrs.find (poa);

	if (it != m_poa_addrs.end ())
	  {
	    (it->second).m_saw_null = enable;
	  }
	else
	  {
	    PoAInInfo tmp = PoAInInfo ();
	    tmp.m_saw_null = enable;
	    m_poa_addrs[poa] = tmp;
	  }
      }

      bool
      IncomingFace::SawNULLonPoA (Address poa)
      {
	std::map<Address,PoAInInfo>::iterator it = m_poa_addrs.find (poa);

	if (it != m_poa_addrs.end ())
	  {
	    return (it->second).m_saw_null;
	  }
	else
	  return false;
      }

      /**
       * @brie Copy operator
       */
      IncomingFace &
      IncomingFace::operator = (const IncomingFace &other)
      {
	m_face = other.m_face;
	m_addrs = other.m_addrs;
	m_poa_addrs = other.m_poa_addrs;
	m_arrivalTime = other.m_arrivalTime;
	m_raw_icn = other.m_raw_icn;
	return *this;
      }
    } // namespace pit
  } // namespace nnn
} // namespace ns3
