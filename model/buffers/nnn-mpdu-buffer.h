/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil -*- */
/*
 * Copyright (c) 2015 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnn-mpdu-buffer.h is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-mpdu-buffer.h is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-mpdu-buffer.h. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#ifndef MPDU_BUFFER_H_
#define MPDU_BUFFER_H_

#include "ns3/traced-value.h"
#include "ns3/trace-source-accessor.h"
#include "ns3/object.h"
#include "ns3/ptr.h"

#include "ns3/nnn-pdus.h"
#include "ns3/nnn-naming.h"
#include "ns3/nnn-trie.h"
#include "ns3/nnn-counting-policy.h"
#include "ns3/nnn-trie-with-policy.h"
#include "ns3/nnn-pdu-buffer-queue.h"

namespace ns3
{
  namespace nnn
  {
    class MPDUBuffer : public Object,
    protected ns3::nnn::nnnSIM::trie_with_policy<
    NNNAddress,
    ns3::nnn::nnnSIM::smart_pointer_payload_traits<PDUQueue>,
    ns3::nnn::nnnSIM::counting_policy_traits
    >
    {
    public:
      typedef ns3::nnn::nnnSIM::trie_with_policy<
	  NNNAddress,
	  ns3::nnn::nnnSIM::smart_pointer_payload_traits<PDUQueue>,
	  ns3::nnn::nnnSIM::counting_policy_traits
	  > super;

      static TypeId GetTypeId (void);

      MPDUBuffer ();

      MPDUBuffer (Time retx);

      virtual
      ~MPDUBuffer ();

      void
      AddDestination (const NNNAddress &addr, Time lease_expire);

      void
      AddDestination (Ptr<NNNAddress> addr, Time lease_expire);

      void
      RemoveDestination (const NNNAddress &addr);

      void
      RemoveDestination (Ptr<NNNAddress> addr);

      bool
      DestinationExists (const NNNAddress &addr);

      bool
      DestinationExists (Ptr<NNNAddress> addr);

      void
      PushDEN (Ptr<NNNAddress> addr, Ptr<const DEN> den_p);

      void
      PushDEN (const NNNAddress &addr, Ptr<const DEN> den_p);

      std::queue<Ptr<Packet> >
      PopQueue (const NNNAddress &addr);

      std::queue<Ptr<Packet> >
      PopQueue (Ptr<NNNAddress> addr);

      uint
      QueueSize (const NNNAddress &addr);

      uint
      QueueSize (Ptr<NNNAddress> addr);

      void
      SetReTX (Time rtx);

      Time
      GetReTX () const;

      void
      UpdateLeaseTime (const NNNAddress &addr, Time lease_expire);

      void
      UpdateLeaseTime (Ptr<NNNAddress> addr, Time lease_expire);

      void
      CleanExpired (const NNNAddress &addr);

    private:
      Time m_retx;
    };

    std::ostream& operator<< (std::ostream& os, const MPDUBuffer &buffer);

  } /* namespace nnn */
} /* namespace ns3 */

#endif /* MPDU_BUFFER_H_ */
