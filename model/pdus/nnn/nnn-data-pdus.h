/* -*- Mode: C++; c-file-style: "gnu" -*- */
/*
 * Copyright (c) 2015 Waseda University, Sato Laboratory
 *
 *   This file is part of nnnsim.
 *
 *  nnn-data-pdus.h is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-data-pdus.h is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-data-pdus.h. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#ifndef _NNN_DATA_PDUS_H_
#define _NNN_DATA_PDUS_H_

#include "ns3/nnn-pdu.h"
#include "ns3/nnn-naming.h"

namespace ns3
{
  namespace nnn
  {
    class DATAPDU : public virtual NNNPDU
    {
    public:
      DATAPDU ();

      virtual
      ~DATAPDU ();

      const NNNAddress&
      GetSrcName () const;

      const NNNAddress&
      GetDstName () const;

      Ptr<const NNNAddress>
      GetSrcNamePtr () const;

      Ptr<const NNNAddress>
      GetDstNamePtr () const;

      void
      SetSrcName (Ptr<const NNNAddress> src);

      void
      SetDstName (Ptr<const NNNAddress> dst);

      /**
       * @brief Gets the payload of the 3N DATA PDU
       */
      Ptr<const Packet>
      GetPayload () const;

      /**
       * @brief Sets the payload of the 3N DATA PDU
       */
      void
      SetPayload (Ptr<Packet> payload);

      /**
       * @brief Get the PDU type in 3N DATA PDU
       */
      uint16_t
      GetPDUPayloadType () const;

      /**
       * @brief Set the PDU type held in 3N DATA PDU
       *
       * @param pdu_type PDU type in 3N DATA PDU
       */
      void
      SetPDUPayloadType (uint16_t pdu_type);

      void
      SetPDURF (bool value);

      void
      SetPDURF (uint8_t value);

      uint8_t
      GetPDURF () const;

      bool
      hasRF () const;

      void
      SetPDUWindow (uint32_t value);

      uint32_t
      GetPDUWindow () const;

      void
      SetRate (uint32_t);

      uint32_t
      GetRate () const;

      void
      SetInitialWindowSeq (uint32_t);

      uint32_t
      GetInitialWindowSeq () const;

      bool
      has3NSrc () const;

      bool
      has3NDst () const;

      virtual void
      Print (std::ostream &os) const;

      virtual Ptr<DATAPDU>
      Copy () const = 0;

    protected:
      uint16_t m_PDUdatatype;   ///< @brief Type of payload held in PDU
      Ptr<Packet> m_payload;    ///< @brief Payload
      Ptr<const NNNAddress> m_src_name;
      Ptr<const NNNAddress> m_dst_name;
      uint8_t m_pdurf;
      uint32_t m_window;
      uint32_t m_rate;
      uint32_t m_ini_seq;
    };

    class DATAPDUException {};
  } /* namespace nnn */
} /* namespace ns3 */

#endif /* _NNN_DATA_PDUS_H_ */
