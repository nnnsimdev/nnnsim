/* -*- Mode:C++; c-file-style:"gnu" -*- */
/*
 * Copyright (c) 2016 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnn-icn-face.h is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-icn-face.h is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-icn-face.h. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jelfn@sislacom.com>
 *
 */
#ifndef NNN_ICN_FACE_H_
#define NNN_ICN_FACE_H_

#include "nnn-face.h"
#include <map>

namespace ns3
{
  namespace nnn
  {
    class Interest;
    class Data;

    class ICN3NForwardingStrategy;

    class ICNFace : public Face
    {
    public:
      // ICN protocol handlers
      typedef Callback<void, Ptr<ICNFace>, Ptr<Interest> > InterestHandler;
      typedef Callback<void, Ptr<ICNFace>, Ptr<Data> > DataHandler;

      static TypeId
      GetTypeId ();

      ICNFace (Ptr<Node> node);
      virtual
      ~ICNFace ();

      virtual void
      RegisterProtocolHandlers (Ptr<ForwardingStrategy> fw);

      virtual void
      UnRegisterProtocolHandlers ();

      virtual bool
      SendInterest (Ptr<const Interest> interest_o);

      virtual bool
      SendData (Ptr<const Data> data_o);

      virtual bool
      ReceiveICN (Ptr<const Packet> p);

      virtual bool
      ReceiveInterest (Ptr<Interest> interest_i);

      virtual bool
      ReceiveData (Ptr<Data> data_i);

      void
      EnableICN (bool enable = true);

      bool
      IsICNEnabled ();

    protected:
      InterestHandler m_upstreamInterestHandler;
      DataHandler m_upstreamDataHandler;
    };
  } /* namespace nnn */
} /* namespace ns3 */

#endif /* NNN_ICN_FACE_H_ */
