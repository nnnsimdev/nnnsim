/* -*- Mode:C++; c-file-style:"gnu" -*- */
/*
 * Copyright (c) 2016 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnn-pure-icn-producer.h is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-pure-icn-producer.h is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-pure-icn-producer.h. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#ifndef NNN_PURE_ICN_PRODUCER_H
#define NNN_PURE_ICN_PRODUCER_H

#include "nnn-pure-icn-app.h"

#include "ns3/log.h"
#include "ns3/packet.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/uinteger.h"
#include "ns3/nnn-icn-name.h"

namespace ns3
{
  namespace nnn
  {

    class PureICNProducer : public PureICNApp
    {
    public:
      static TypeId
      GetTypeId (void);

      PureICNProducer ();

      virtual
      ~PureICNProducer ();

      Ptr<Data>
      CreateReturnData (Ptr<const Interest> interest);

      void
      OnInterest (Ptr<const Interest> interest);

      void
      OnICN (Ptr<const Packet> pkt);

    protected:
      virtual void
      StartApplication ();

      virtual void
      StopApplication ();

    private:
      icn::Name m_prefix;
      icn::Name m_keyLocator;
      uint32_t m_virtualPayloadSize;
      Time m_freshness;
      uint32_t m_signature;
    };
  } /* namespace nnn */
} /* namespace ns3 */

#endif /* NNN_PURE_ICN_PRODUCER_H */
