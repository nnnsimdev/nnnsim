/* -*- Mode:C++; c-file-style:"gnu" -*- */
/*
 * Copyright (c) 2016 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnn-pure-icn-app.cc is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-pure-icn-app.cc is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnnn-pure-icn-app.cc. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#include "nnn-pure-icn-app.h"

#include "ns3/boolean.h"
#include "ns3/assert.h"
#include "ns3/log.h"
#include "ns3/packet.h"
#include "ns3/ptr.h"
#include "ns3/string.h"

#include "ns3/nnn-icn-app-face.h"
#include "ns3/nnn-l3-protocol.h"
#include "ns3/nnn-forwarding-strategy.h"
#include "ns3/nnn-icn-header-helper.h"

namespace ns3
{
  NS_LOG_COMPONENT_DEFINE ("nnn.PureICNApp");

  namespace nnn
  {
    TypeId
    PureICNApp::GetTypeId (void)
    {
      static TypeId tid = TypeId ("ns3::nnn::PureICNApp")
	.SetGroupName ("Nnn")
	.SetParent<Application> ()
	.AddConstructor<PureICNApp> ()

	.AddTraceSource ("ReceivedInterests", "ReceivedInterests",
			 MakeTraceSourceAccessor (&PureICNApp::m_receivedInterests),
			 "ns3::nnn:PureICNApp::ReceivedInterestTracedCallback")

	.AddTraceSource ("ReceivedNacks", "ReceivedNacks",
			 MakeTraceSourceAccessor (&PureICNApp::m_receivedNacks),
			 "ns3::nnn:PureICNApp::ReceivedNackTracedCallback")

	.AddTraceSource ("ReceivedDatas", "ReceivedDatas",
			 MakeTraceSourceAccessor (&PureICNApp::m_receivedDatas),
			 "ns3::nnn:PureICNApp::ReceivedDataTracedCallback")

	.AddTraceSource ("TransmittedInterests", "TransmittedInterests",
			 MakeTraceSourceAccessor (&PureICNApp::m_transmittedInterests),
			 "ns3::nnn:PureICNApp::TransmittedInterestTracedCallback")

	.AddTraceSource ("TransmittedDatas", "TransmittedDatas",
			 MakeTraceSourceAccessor (&PureICNApp::m_transmittedDatas),
			 "ns3::nnn:PureICNApp::TransmittedDataTracedCallback")

	.AddTraceSource("BecameMobile", "Specifies if the ICN Application is mobile",
			MakeTraceSourceAccessor (&PureICNApp::m_mobility),
			"ns3::nnn::PureICNApp::AppBoolTracedCallback")

	.AddAttribute ("use3N", "Flags that the PureICNApp takes advantage of 3N capabilities",
			BooleanValue (true),
			MakeBooleanAccessor(&PureICNApp::m_use3N),
			MakeBooleanChecker ())

	.AddAttribute ("IsMobile", "Flags that the PureICNApp is mobile",
			 BooleanValue (false),
			 MakeBooleanAccessor(&PureICNApp::IsMobile, &PureICNApp::SetMobile),
			 MakeBooleanChecker ())
	;
      return tid;
    }

    PureICNApp::PureICNApp ()
    : m_active              (false)
    , m_face                (0)
    , m_use3N               (true)
    , m_isMobile            (false)
    {
    }

    PureICNApp::~PureICNApp ()
    {
    }

    uint32_t
    PureICNApp::GetId () const
    {
      if (m_face == 0)
	return (uint32_t)-1;
      else
	return m_face->GetId ();
    }

    void
    PureICNApp::SetMobile(bool value)
    {
      m_isMobile = value;
      m_mobility(m_isMobile);
    }

    bool
    PureICNApp::IsMobile() const
    {
      return m_isMobile;
    }

    void
    PureICNApp::OnInterest (Ptr<const Interest> interest)
    {
      NS_LOG_FUNCTION (this << interest);
      m_receivedInterests (interest, this, m_face);
    }

    void
    PureICNApp::OnNack (Ptr<const Interest> interest)
    {
      NS_LOG_FUNCTION (this << interest);
      m_receivedNacks (interest, this, m_face);
    }

    void
    PureICNApp::OnData (Ptr<const Data> data)
    {
      NS_LOG_FUNCTION (this << data);
      m_receivedDatas (data, this, m_face);
    }

    void
    PureICNApp::OnICN(Ptr<const Packet> pkt)
    {
      NS_LOG_FUNCTION (this);
    }

    void
    PureICNApp::DoDispose (void)
    {
      NS_LOG_FUNCTION_NOARGS ();
      Application::DoDispose ();
    }

    // Application Methods
    void
    PureICNApp::StartApplication () // Called at time specified by Start
    {
      NS_LOG_FUNCTION_NOARGS ();

      NS_ASSERT (m_active != true);
      m_active = true;

      NS_ASSERT_MSG (GetNode ()->GetObject<L3Protocol> () != 0,
		     "3N stack should be installed on the node " << GetNode ());

      // Create the face
      NS_LOG_DEBUG ("Creating ICNAppFace for PureICNApp");
      m_face = CreateObject<ICNAppFace> (this);

      NS_LOG_DEBUG ("Disabling 3N access to App");
      m_face->EnableApp3N(false);

      // Add the face to the 3N stack
      NS_LOG_DEBUG ("Connecting ICNAppFace to L3Protocol");
      GetNode ()->GetObject<L3Protocol> ()->AddFace (m_face);

      // Turn the face up
      m_face->SetUp (true);
    }

    void
    PureICNApp::StopApplication () // Called at time specified by Stop
    {
      NS_LOG_FUNCTION_NOARGS ();

      if (!m_active) return; //don't assert here, just return

      NS_ASSERT (GetNode ()->GetObject<L3Protocol> () != 0);

      m_active = false;

      // step 1. Disable face
      m_face->SetUp (false);

      // step 2. Remove face from Nnn stack
      GetNode ()->GetObject<L3Protocol> ()->RemoveFace (m_face);

      // step 3. Destroy face
      if (m_face->GetReferenceCount () != 1)
	{
	  NS_LOG_ERROR ("At this point, nobody else should have referenced this face, but we have "
	      << m_face->GetReferenceCount () << " references");
	}
      m_face = 0;
    }
  } /* namespace nnn */
} /* namespace ns3 */
