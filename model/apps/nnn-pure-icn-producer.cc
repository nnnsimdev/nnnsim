/* -*- Mode:C++; c-file-style:"gnu" -*- */
/*
 * Copyright (c) 2016 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnn-pure-icn-producer.cc is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-pure-icn-producer.cc is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-pure-icn-producer.cc. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#include "nnn-pure-icn-producer.h"

#include "ns3/nnn-fib.h"
#include "ns3/nnn-fib-entry.h"
#include "ns3/nnn-icn-app-face.h"
#include "ns3/nnn-icn-header-helper.h"
#include "ns3/nnn-fw-hop-count-tag.h"

namespace ns3
{
  NS_LOG_COMPONENT_DEFINE ("nnn.PureICNProducer");

  namespace nnn
  {
    NS_OBJECT_ENSURE_REGISTERED (PureICNProducer);

    TypeId
    PureICNProducer::GetTypeId (void)
    {
      static TypeId tid = TypeId ("ns3::nnn::PureICNProducer")
	  .SetGroupName ("Nnn")
	  .SetParent<PureICNApp> ()
	  .AddConstructor<PureICNProducer> ()
	  .AddAttribute ("Prefix","Prefix, for which producer has the data",
			 StringValue ("/"),
			 MakeNameAccessor (&PureICNProducer::m_prefix),
			 icn::MakeNameChecker ())
	  .AddAttribute ("PayloadSize", "Virtual payload size for Content packets",
			 UintegerValue (1024),
			MakeUintegerAccessor (&PureICNProducer::m_virtualPayloadSize),
			MakeUintegerChecker<uint32_t> ())
	  .AddAttribute ("Freshness", "Freshness of data packets, if 0, then unlimited freshness",
			 TimeValue (Seconds (0)),
			 MakeTimeAccessor (&PureICNProducer::m_freshness),
			 MakeTimeChecker ())
	  .AddAttribute ("Signature", "Fake signature, 0 valid signature (default), other values application-specific",
			 UintegerValue (0),
			 MakeUintegerAccessor (&PureICNProducer::m_signature),
			 MakeUintegerChecker<uint32_t> ())
	  .AddAttribute ("KeyLocator", "Name to be used for key locator.  If root, then key locator is not used",
			 icn::NameValue (),
			 MakeNameAccessor (&PureICNProducer::m_keyLocator),
			 icn::MakeNameChecker ())
			 ;
      return tid;
    }

    PureICNProducer::PureICNProducer ()
    {
    }

    PureICNProducer::~PureICNProducer ()
    {
    }

    Ptr<Data>
    PureICNProducer::CreateReturnData (Ptr<const Interest> interest)
    {
      NS_LOG_FUNCTION (this << interest);

      Ptr<Data> data = Create<Data> (Create<Packet> (m_virtualPayloadSize));
      Ptr<icn::Name> dataName = Create<icn::Name> (interest->GetName ());
      data->SetName (dataName);
      data->SetFreshness (m_freshness);
      data->SetTimestamp (Simulator::Now());

      data->SetSignature (m_signature);
      if (m_keyLocator.size () > 0)
	{
	  data->SetKeyLocator (Create<icn::Name> (m_keyLocator));
	}

      NS_LOG_INFO ("Responding with Pkt: " << *dataName << " Interest Name: " << data->GetName () << " SeqNo " << std::dec << data->GetName ().get (-1).toSeqNum ());

      // Echo back FwHopCountTag if exists
      FwHopCountTag hopCountTag;
      if (interest->GetPayload ()->PeekPacketTag (hopCountTag))
	{
	  data->GetPayload ()->AddPacketTag (hopCountTag);
	}

      return data;
    }

    void
    PureICNProducer::OnInterest (Ptr<const Interest> interest)
    {
      NS_LOG_FUNCTION (this << interest);
      PureICNApp::OnInterest (interest);

      Ptr<Data> data = CreateReturnData (interest);

      bool ok = false;

      if (m_use3N)
	{
	  Ptr<Packet> pkt = icn::Wire::FromData(data);
	  ok = m_face->SendIn3NPDU (pkt);
	}
      else
	{
	  ok = m_face->ReceiveData (data);
	}

      if (ok)
	{
	  m_transmittedDatas (data, this, m_face);
	}
    }

    void
    PureICNProducer::OnICN (Ptr<const Packet> pkt)
    {
      NS_LOG_FUNCTION (this);
      bool receivedInterest =false;
      bool receivedData = false;
      Ptr<Interest> interest;
      Ptr<Data> data;

      Ptr<Packet> icn_pdu = pkt->Copy();
      try {
	  icn::HeaderHelper::Type icnType = icn::HeaderHelper::GetICNHeaderType(icn_pdu);
	  switch (icnType)
	  {
	    case icn::HeaderHelper::INTEREST_ICN:
	      interest = ns3::icn::Wire::ToInterest (icn_pdu, ns3::icn::Wire::WIRE_FORMAT_NDNSIM);
	      receivedInterest = true;
	      break;
	    case icn::HeaderHelper::CONTENT_OBJECT_ICN:
	      data = ns3::icn::Wire::ToData (icn_pdu, ns3::icn::Wire::WIRE_FORMAT_NDNSIM);
	      receivedData = true;
	      break;
	    default:
	      NS_FATAL_ERROR ("Not supported ICN header");
	  }
      }
      catch (icn::UnknownHeaderException)
      {
	  NS_FATAL_ERROR ("Unknown ICN header. Should not happen");
      }

      // If the PDU is an Interest
      if (receivedInterest)
	{
	  uint8_t res = interest->GetInterestType();
	  // Within the Interests, we have possible NACKs, separate
	  if (Interest::MAP_ME_INTEREST_UPDATE <= res && res < Interest::NACK_LOOP)
	    {
	      if (res == Interest::MAP_ME_INTEREST_UPDATE_ACK)
		{
		  NS_LOG_INFO ("Obtained an Interest with MapMe ACK for " << interest->GetName ());
		}
	      else
		{
		  NS_LOG_INFO ("Obtained an Interest with MapMe information for " << interest->GetName ());
		}
	    }
	  else if (Interest::NACK_LOOP <= res)
	    {
	      NS_LOG_INFO ("Obtained an Interest with NACK information");
	      OnNack (interest);
	    }
	  else
	    {
	      NS_LOG_INFO ("Obtained a normal Interest packet");
	      OnInterest (interest);
	    }
	}

      // If the PDU is Data
      if (receivedData)
	{
	  NS_LOG_INFO ("Received Data");
	  OnData (data);
	}
    }

    void
    PureICNProducer::StartApplication ()
    {
      NS_LOG_FUNCTION_NOARGS ();
      NS_ASSERT (GetNode ()->GetObject<Fib> () != 0);

      PureICNApp::StartApplication ();

      Ptr<Fib> fib = GetNode ()->GetObject<Fib> ();

      Ptr<fib::Entry> fibEntry = fib->Add (m_prefix, m_face, 0);

      fibEntry->UpdateStatus (m_face, fib::FaceMetric::ICN_FIB_GREEN);

      NS_LOG_INFO ("Finished setting up Producer application");
    }

    void
    PureICNProducer::StopApplication ()
    {
      NS_LOG_FUNCTION_NOARGS ();

      PureICNApp::StopApplication ();
    }
  } /* namespace nnn */
} /* namespace ns3 */
