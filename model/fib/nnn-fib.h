/* -*- Mode:C++; c-file-style:"gnu" -*- */
/*
 * Copyright (c) 2015 Waseda University, Sato Laboratory
 *
 *   This file is part of nnnsim.
 *
 *  nnn-fib.h is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-fib.h is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-fib.h. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#ifndef _NNN_FIB_H_
#define	_NNN_FIB_H_

#include <boost/format.hpp>

#include "ns3/node.h"
#include "ns3/simple-ref-count.h"
#include "ns3/traced-callback.h"

namespace ns3
{
  namespace icn {
    class Name;
  }

  namespace nnn
  {
    namespace fib {
      class Entry;
    }

    class Interest;
    class Face;
    class NNNAddress;
    typedef Interest InterestHeader;

    /**
     * @ingroup nnn
     * @defgroup nnn-fib FIB
     */
    //                                Blank|3N|Part|Inserted|Mod part|Last seen
    const boost::format fib_formatter("%-8s|%-20s|%-10x|%-12s|%-12s|%-13s\n");
    //                                Blank|FC|PoA|Part|Inserted|Mod part|Last seen
    const boost::format fib_poa_title_formatter("%-2s|%-2s|%-23s|%-10s|%-12s|%-12s|%-13s\n");
    const boost::format fib_poa_formatter("%-2s|%-2x|%-23s|%-10x|%-12s|%-12s|%-13s\n");
    //
    const boost::format fib_no_formatter("%-8s|%-20s|%-10s|%-12s|%-12s|%-13s\n");
    const boost::format fibfm_formatter("%-2x %-8x %-2s %-4x");
    /**
     * @ingroup nnn-fib
     * @brief Class implementing FIB functionality
     */
    class Fib : public Object
    {
    public:
      /**
       * \brief Interface ID
       *
       * \return interface ID
       */
      static TypeId GetTypeId ();
      /**
       * @brief Default constructor
       */
      Fib () {}

      /**
       * @brief Virtual destructor
       */
      virtual ~Fib () { };

      /**
       * \brief Perform longest prefix match
       *
       * \todo Implement exclude filters
       *
       * \param interest Interest packet header
       * \returns If entry found a valid iterator (Ptr<fib::Entry>) will be returned, otherwise End () (==0)
       */
      virtual Ptr<fib::Entry>
      LongestPrefixMatch (const Interest &interest) = 0;

      virtual Ptr<fib::Entry>
      LongestPrefixMatch (Ptr<const icn::Name> interest) = 0;

      /**
       * @brief Get FIB entry for the prefix (exact match)
       *
       * @param prefix Name for FIB entry
       * @returns If entry is found, a valid iterator (Ptr<fib::Entry>) will be returned. Otherwise End () (==0)
       */
      virtual Ptr<fib::Entry>
      Find (const icn::Name &prefix) = 0;

      /**
       * \brief Add or update FIB entry
       *
       * If the entry exists, metric will be updated. Otherwise, new entry will be created
       *
       * @param name	Prefix
       * @param face	Forwarding face
       * @param metric	Routing metric
       */
      virtual Ptr<fib::Entry>
      Add (const icn::Name &prefix, Ptr<Face> face, int32_t metric) = 0;

      /**
       * \brief Add or update FIB entry using smart pointer to prefix
       *
       * If the entry exists, metric will be updated. Otherwise, new entry will be created
       *
       * @param name	Smart pointer to prefix
       * @param face	Forwarding face
       * @param metric	Routing metric
       */
      virtual Ptr<fib::Entry>
      Add (const Ptr<const icn::Name> &prefix, Ptr<Face> face, int32_t metric) = 0;

      virtual Ptr<fib::Entry>
      Add (const icn::Name &prefix, Ptr<Face> face, int32_t metric, const Ptr<const NNNAddress> &addr) = 0;

      virtual Ptr<fib::Entry>
      Add (const Ptr<const icn::Name> &prefix, Ptr<Face> face, int32_t metric, const Ptr<const NNNAddress> &addr) = 0;

      virtual Ptr<fib::Entry>
      Add (const icn::Name &prefix, Ptr<Face> face, int32_t metric, const Address& from) = 0;

      virtual Ptr<fib::Entry>
      Add (const Ptr<const icn::Name> &prefix, Ptr<Face> face, int32_t metric, const Address& from) = 0;

      virtual Ptr<fib::Entry>
      Add (const icn::Name &prefix, Ptr<Face> face, int32_t metric, const Ptr<const NNNAddress> &addr, const Address& from) = 0;

      virtual void
      Update3NSrcDestination (const Ptr<const icn::Name> &prefix, const Ptr<const NNNAddress> &addr, uint32_t lower_part) = 0;

      virtual void
      UpdatePoASrcDestination (const Ptr<const icn::Name> &prefix, Ptr<Face> Face, const Address& addr, uint32_t low_part, int32_t metric) = 0;

      /**
       * @brief Remove FIB entry
       *
       * ! ATTENTION ! Use with caution.  All PIT entries referencing the corresponding FIB entry will become invalid.
       * So, simulation may crash.
       *
       * @param name	Smart pointer to prefix
       */
      virtual void
      Remove (const Ptr<const icn::Name> &prefix) = 0;

      /**
       * @brief Invalidate all FIB entries
       */
      virtual void
      InvalidateAll () = 0;

      /**
       * @brief Remove all references to a face from FIB.  If for some enty that face was the only element,
       * this FIB entry will be removed.
       */
      virtual void
      RemoveFromAll (Ptr<Face> face) = 0;

      /**
       * @brief Print out entries in FIB
       */
      virtual void
      Print (std::ostream &os) const = 0;

      /**
       * @brief Get number of entries in FIB
       */
      virtual uint32_t
      GetSize () const = 0;

      /**
       * @brief Return first element of FIB (no order guaranteed)
       */
      virtual Ptr<const fib::Entry>
      Begin () const = 0;

      /**
       * @brief Return first element of FIB (no order guaranteed)
       */
      virtual Ptr<fib::Entry>
      Begin () = 0;

      /**
       * @brief Return item next after last (no order guaranteed)
       */
      virtual Ptr<const fib::Entry>
      End () const = 0;

      /**
       * @brief Return item next after last (no order guaranteed)
       */
      virtual Ptr<fib::Entry>
      End () = 0;

      /**
       * @brief Advance the iterator
       */
      virtual Ptr<const fib::Entry>
      Next (Ptr<const fib::Entry>) const = 0;

      /**
       * @brief Advance the iterator
       */
      virtual Ptr<fib::Entry>
      Next (Ptr<fib::Entry>) = 0;

      ////////////////////////////////////////////////////////////////////////////
      ////////////////////////////////////////////////////////////////////////////
      ////////////////////////////////////////////////////////////////////////////

      /**
       * @brief Static call to cheat python bindings
       */
      static inline Ptr<Fib>
      GetFib (Ptr<Object> node);

      ////////////////////////////////////////////////////////////////////////////
      ////////////////////////////////////////////////////////////////////////////
      ////////////////////////////////////////////////////////////////////////////

      typedef void (* EntriesTracedCallback)
	  (const Ptr<const icn::Name>, const size_t);


    protected:
      TracedCallback<Ptr<const icn::Name>, size_t> m_icn_part_entries_trace;
      TracedCallback<Ptr<const icn::Name>, size_t> m_icn_part_poa_entries_trace;

    private:
      Fib (const Fib&) {} ; ///< \brief copy constructor is disabled
    };

    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////

    std::ostream& operator<< (std::ostream& os, const Fib &fib);

    Ptr<Fib>
    Fib::GetFib (Ptr<Object> node)
    {
      return node->GetObject<Fib> ();
    }
  } // namespace nnn
} // namespace ns3

#endif // _NNN_FIB_H_
