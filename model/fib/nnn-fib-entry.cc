/* -*- Mode:C++; c-file-style:"gnu" -*- */
/*
 * Copyright (c) 2015 Waseda University, Sato Laboratory
 *
 *   This file is part of nnnsim.
 *
 *  nnn-fib-entry.cc is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-fib-entry.cc is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-fib-entry.cc. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#include "ns3/log.h"
#include "ns3/simulator.h"

#define ICN_RTO_ALPHA 0.125
#define ICN_RTO_BETA 0.25
#define ICN_RTO_K 4

#include "ns3/nnn-icn-naming.h"

#include <boost/lambda/bind.hpp>
#include <boost/lambda/lambda.hpp>
#include <boost/ref.hpp>

namespace ll = boost::lambda;

#include "nnn-fib.h"
#include "nnn-fib-entry.h"

namespace ns3
{
  NS_LOG_COMPONENT_DEFINE ("nnn.fib.Entry");

  namespace nnn
  {
    namespace fib
    {
      //////////////////////////////////////////////////////////////////////
      // Helpers
      //////////////////////////////////////////////////////////////////////

      void
      FaceMetric::UpdateRtt (const Time &rttSample)
      {
	// const Time & this->m_rttSample

	//update srtt and rttvar (RFC 2988)
	if (m_sRtt.IsZero ())
	  {
	    //first RTT measurement
	    NS_ASSERT_MSG (m_rttVar.IsZero (), "SRTT is zero, but variation is not");

	    m_sRtt = rttSample;
	    m_rttVar = Time (m_sRtt / 2.0);
	  }
	else
	  {
	    m_rttVar = Time ((1 - ICN_RTO_BETA) * m_rttVar + 1.0 * ICN_RTO_BETA * Abs(m_sRtt - rttSample));
	    m_sRtt = Time ((1 - ICN_RTO_ALPHA) * m_sRtt + 1.0 * ICN_RTO_ALPHA * rttSample);
	  }

	m_lastSeen = Simulator::Now ();
      }

      /////////////////////////////////////////////////////////////////////

      void
      Entry::UpdateFaceRtt (Ptr<Face> face, const Time &sample)
      {
	faces_by_ptr::iterator record = m_faces.get<i_face> ().find (face);
	if (record == m_faces.get<i_face> ().end ())
	  {
	    return;
	  }

	m_faces.modify (record,
			ll::bind (&FaceMetric::UpdateRtt, ll::_1, sample));

	// reordering random access index same way as by metric index
	m_faces.get<i_nth> ().rearrange (m_faces.get<i_metric> ().begin ());
      }

      void
      Entry::Update3NSrcDestination (Ptr<const NNNAddress> addr)
      {
	NS_LOG_FUNCTION (this << *addr);

	NS_LOG_DEBUG ("Searching in seen SRC 3N names for " << *addr);

	parts_seen_by_name& names_index = m_src_addrs.get<p_3name> ();
	parts_seen_by_name::iterator it0, it1;
	std::tie(it0, it1) = names_index.equal_range(addr);

	if (it0 != it1)
	  {
	    // Create a vector with the iterators
	    std::vector<parts_seen_by_name::iterator> parts;

	    for ( ; it0 != it1; ++it0)
	      {
		parts.push_back(it0);
	      }

	    for (size_t num = 0; num < parts.size (); ++num)
	      {
		Part3NSeen tmp = *(parts[num]);
		NS_LOG_DEBUG ("Found entry for 3N name: " << *(tmp.m_name) <<
			      " Part: " << tmp.m_part <<
			      " Inserted: " << tmp.m_firstInserted.GetSeconds() <<
			      " Last seen: " << tmp.m_lastSeen.GetSeconds() <<
			      " updating to: " << Simulator::Now ().GetSeconds ());

		names_index.modify (parts[num],
				     ll::bind (&Part3NSeen::UpdateLastSeen, ll::_1));
	      }
	  }
	else
	  {
	    NS_LOG_DEBUG ("No entry for " << *addr << ", creating");
	    m_src_addrs.insert(Part3NSeen (addr));
	  }
      }

      bool
      Entry::Update3NSrcDestination (Ptr<const NNNAddress> addr, uint32_t low_part)
      {
	NS_LOG_FUNCTION (this << *addr << low_part);

	return UpdatePartsSeenContainer(0, addr, low_part);
      }

      void
      Entry::Update3NPossibleDestination (Ptr<const NNNAddress> addr)
      {
	NS_LOG_FUNCTION (this << *addr);

	NS_LOG_DEBUG ("Searching in seen DST 3N names for " << *addr);

	parts_seen_by_name& names_index = m_dst_addrs.get<p_3name> ();
	parts_seen_by_name::iterator it0, it1;
	std::tie(it0, it1) = names_index.equal_range(addr);

	if (it0 != it1)
	  {
	    // Create a vector with the iterators
	    std::vector<parts_seen_by_name::iterator> parts;

	    for ( ; it0 != it1; ++it0)
	      {
		parts.push_back(it0);
	      }

	    for (size_t num = 0; num < parts.size (); ++num)
	      {
		Part3NSeen tmp = *(parts[num]);
		NS_LOG_DEBUG ("Found entry for 3N name: " << *(tmp.m_name) <<
			      " Part: " << tmp.m_part <<
			      " Inserted: " << tmp.m_firstInserted.GetSeconds() <<
			      " Last seen: " << tmp.m_lastSeen.GetSeconds() <<
			      " updating to: " << Simulator::Now ().GetSeconds ());

		names_index.modify (parts[num],
				    ll::bind (&Part3NSeen::UpdateLastSeen, ll::_1));
	      }
	  }
	else
	  {
	    NS_LOG_DEBUG ("No entry for " << *addr << ", creating");
	    m_dst_addrs.insert (Part3NSeen (addr));
	  }
      }

      void
      Entry::Update3NPossibleDestination (Ptr<const NNNAddress> addr, uint32_t low_part)
      {
	NS_LOG_FUNCTION (this << *addr << low_part);

	UpdatePartsSeenContainer(1, addr, low_part);
      }

      void
      Entry::PrintDebug (int st)
      {
	PartsSeenContainer *con = 0;

	if (st == 0)
	  {
	    NS_LOG_FUNCTION (this << "source");
	    con = &m_src_addrs;
	  }
	else if (st == 1)
	  {
	    NS_LOG_FUNCTION (this << "destination");
	    con = &m_dst_addrs;
	  }

	size_t con_size = con->size ();
	if (con_size > 0)
	  {
	    NS_LOG_DEBUG ("Container size: " << con_size << std::endl <<
			  "        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%" <<
			  std::endl << boost::format(fib_formatter) % "" % "Auth 3N" % "" % "" % "" % "" <<
			  "        -----------------------------------------------------------------------" << std::endl);

	    parts_seen_by_natural_order::iterator seen_index1 = con->get<p_natural> ().begin();

	    while (seen_index1 != con->get<p_natural> ().end ())
	      {
		NS_LOG_DEBUG ("" << std::endl << boost::format(fib_formatter) % "" % *(seen_index1->m_name) %
		    seen_index1->m_part % (seen_index1->m_firstInserted).GetSeconds () %
		    (seen_index1->m_partmod).GetSeconds () % (seen_index1->m_lastSeen).GetSeconds());
		++seen_index1;
	      }
	    NS_LOG_DEBUG ("" << std::endl << "        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
	  }
	else
	  {
	    NS_LOG_DEBUG ("No data!");
	  }
      }

      bool
      Entry::UpdatePartsSeenContainer (int st, Ptr<const NNNAddress> addr, uint32_t low_part)
      {
	PartsSeenContainer *con;

	if (st == 0)
	  {
	    NS_LOG_FUNCTION (this << "source" << *addr << low_part);
	    con = &m_src_addrs;
	  }
	else if (st == 1)
	  {
	    NS_LOG_FUNCTION (this << "destination" << *addr << low_part);
	    con = &m_dst_addrs;
	  }

	parts_seen_by_name& names_index = con->get<p_3name> ();
	parts_seen_by_name::iterator it0, it1;
	std::tie(it0, it1) = names_index.equal_range(addr);

	parts_seen_by_natural_order& order_index = con->get<p_natural> ();

	size_t total_parts = con->size ();

	if (it0 != it1)
	  {
	    // Create a vector with the iterators
	    std::vector<parts_seen_by_name::iterator> parts;

	    // Keep list of all iterators
	    for ( ; it0 != it1; ++it0)
	      {
		parts.push_back(it0);
	      }

	    size_t total_found_parts = parts.size ();

	    NS_LOG_DEBUG ("Search for " << *addr << " entries returned " << total_found_parts << " entries");

	    // First update all the times - this is necessary for the following search
	    for (size_t num = 0; num < total_found_parts; num++)
	      {
		Part3NSeen tmp = *(parts[num]);
//		NS_LOG_DEBUG ("Found entry for 3N name: " << *(tmp.m_name) <<
//			      " Part: " << tmp.m_part <<
//			      " Inserted: " << tmp.m_firstInserted.GetSeconds () <<
//			      " Last seen: " << tmp.m_lastSeen.GetSeconds ());

		names_index.modify (parts[num],
				    ll::bind (&Part3NSeen::UpdateLastSeen, ll::_1));
	      }

	    Part3NSeen tmp2 = *(parts[total_found_parts-1]);
	    parts_seen_by_natural_order::iterator curr = order_index.find (tmp2);

	    size_t vec_pos = total_found_parts-1;
	    size_t min_pos = std::distance(order_index.begin (), curr);
	    uint32_t min_max_part = tmp2.m_part;

	    parts_seen_by_natural_order::iterator check = order_index.begin ();
	    for (size_t loc = 1; loc < min_pos; loc++)
	      {
		++check;
	      }

	    uint32_t max_max_part = check->m_part;
	    NS_LOG_DEBUG ("Minimum part is " << min_max_part << " with position " << min_pos << " of " << total_parts << ". Previous entry has part " << max_max_part);

	    if (low_part < min_max_part)
	      {
		Part3NSeen tmp = *(parts[vec_pos]);
		NS_LOG_DEBUG ("Found " << *(tmp.m_name) <<
			      " Part: " << tmp.m_part <<
			      " Inserted: " << tmp.m_firstInserted.GetSeconds () <<
			      " Last seen: " << tmp.m_lastSeen.GetSeconds () <<
			      " modifying Part to: " << low_part);

		names_index.modify (parts[vec_pos],
				    ll::bind (&Part3NSeen::UpdatePart, ll::_1, low_part));

		parts_seen_by_natural_order::iterator i_start, i_stop, i_curr = order_index.begin ();

		Ptr<const NNNAddress> name_1 = 0;
		Ptr<const NNNAddress> name_2 = 0;
		uint32_t part_1 = 0;
		uint32_t part_2 = 0;

//		NS_LOG_DEBUG ("Compressing resulting initial table: ");
//		PrintDebug (st);

		while (i_curr != order_index.end ())
		  {
		    i_start = i_curr;
		    if (i_start != order_index.end ())
		      {
			i_curr++;
			if (i_curr != order_index.end ())
			  {
			    i_stop = i_curr;
			  }
			else
			  {
			    break;
			  }
		      }
		    else
		      {
			break;
		      }

		    name_1 = i_start->m_name;
		    name_2 = i_stop->m_name;

		    part_1 = i_start->m_part;
		    part_2 = i_stop->m_part;

//		    NS_LOG_DEBUG ("Verifying 3N name: " << *(name_1) << " Part: " << part_1 <<
//				  " vs 3N name: " << *(name_2) << " Part: " << part_2);

		    if (*name_1 == *name_2)
		      {
			if (part_1 >= part_2)
			  {
			    NS_LOG_DEBUG ("Eliminating 1st 3N name: " << *(name_1) << " Part: " << part_1);
			    i_curr = order_index.erase (i_start);
			  }
			else
			  {
			    NS_LOG_DEBUG ("Eliminating 2nd 3N name: " << *(name_2) << " Part: " << part_2);
			    i_curr = i_start;
			    order_index.erase (i_stop);
			  }
		      }
		    else
		      {
			if (part_1 == part_2)
			  {
			    NS_LOG_DEBUG ("Eliminating 1st 3N name: " << *(name_1) << " Part: " << part_1 << " due to repeat part number");
			    i_curr = order_index.erase (i_start);
			  }
		      }
		  }

//		NS_LOG_DEBUG ("Compressed table: ");
//		PrintDebug (st);
		return true;
	      }
	    else
	      {
		if ((min_pos > 0) && (low_part > max_max_part))
		  {
		    NS_LOG_DEBUG ("Due to entry position being at " << min_pos << " of " <<
				  total_parts <<  " and previous part being " <<
				  max_max_part << " (<) inserting new " <<
				  *addr << " using Part: " << low_part);
		    con->insert(Part3NSeen (addr, low_part));
		    return true;
		  }
		else
		  {
		    NS_LOG_DEBUG ("No modifications required to parts from update " << *addr << " part: " << low_part);
		    return false;
		  }
	      }
	  }
	else
	  {
	    NS_LOG_DEBUG ("No entry for " << *addr << ", creating with " << low_part);
	    con->insert(Part3NSeen (addr, low_part));
	    return true;
	  }
      }

      void
      Entry::UpdatePoADestination (Ptr<Face> face, const Address& addr, int32_t metric)
      {
	NS_LOG_FUNCTION (face->GetId () << addr << metric);

	faces_by_ptr::iterator record = m_faces.get<i_face> ().find (face);
	if (record == m_faces.get<i_face> ().end ())
	  {
	    NS_LOG_INFO ("Face " << face->GetId () << " not found, adding!");
	    m_faces.insert (FaceMetric (face, addr, metric));
	  }
	else
	  {
	    NS_LOG_INFO ("Face " << face->GetId () << " found, updating");

	    Address tmp = addr;
	    m_faces.modify (record,
			    ll::bind (&FaceMetric::AddPoA, ll::_1, tmp, metric));
	  }
      }

      bool
      Entry::UpdatePoADestination (Ptr<Face> face, const Address& addr, uint32_t low_part, int32_t metric)
      {
	NS_LOG_FUNCTION (face->GetId () << addr << low_part << metric);

	UpdatePoADestination (face, addr, metric);

	parts_poa_seen_by_poa& poa_index = m_src_poa_addrs.get<p_poa> ();
	parts_poa_seen_by_poa::iterator it0, it1;
	std::tie(it0, it1) = poa_index.equal_range(addr);

	parts_poa_seen_by_natural_order& order_index = m_src_poa_addrs.get<p_natural> ();

	size_t total_parts = m_src_poa_addrs.size ();

	if (it0 != it1)
	  {
	    // Create a vector with the iterators
	    std::vector<parts_poa_seen_by_poa::iterator> parts;

	    // Keep list of all iterators
	    for ( ; it0 != it1; ++it0)
	      {
		if (it0->m_face)
		  {
		    if (it0->m_face->GetId () == face->GetId ())
		      {
			parts.push_back(it0);
		      }
		  }
	      }

	    size_t total_found_parts = parts.size ();

	    NS_LOG_DEBUG ("Search for " << addr << " entries returned " << total_found_parts << " entries");

	    // First update all the times - this is necessary for the following search
	    for (size_t num = 0; num < total_found_parts; num++)
	      {
		PartPoASeen tmp = *(parts[num]);
		//		NS_LOG_DEBUG ("Found entry for 3N name: " << *(tmp.m_name) <<
		//			      " Part: " << tmp.m_part <<
		//			      " Inserted: " << tmp.m_firstInserted.GetSeconds () <<
		//			      " Last seen: " << tmp.m_lastSeen.GetSeconds ());

		poa_index.modify (parts[num],
				    ll::bind (&PartPoASeen::UpdateLastSeen, ll::_1));
	      }

	    PartPoASeen tmp2 = *(parts[total_found_parts-1]);
	    parts_poa_seen_by_natural_order::iterator curr = order_index.find (tmp2);

	    size_t vec_pos = total_found_parts-1;
	    size_t min_pos = std::distance(order_index.begin (), curr);
	    uint32_t min_max_part = tmp2.m_part;

	    parts_poa_seen_by_natural_order::iterator check = order_index.begin ();
	    for (size_t loc = 1; loc < min_pos; loc++)
	      {
		++check;
	      }

	    uint32_t max_max_part = check->m_part;
	    NS_LOG_DEBUG ("Minimum part is " << min_max_part << " with position " << min_pos << " of " << total_parts <<
			  ". Previous entry has part " << max_max_part);

	    if (low_part < min_max_part)
	      {
		PartPoASeen tmp = *(parts[vec_pos]);
		NS_LOG_DEBUG ("Found Face: " << tmp.m_face->GetId () <<
			      " PoA: " << tmp.m_poa <<
			      " Part: " << tmp.m_part <<
			      " Inserted: " << tmp.m_firstInserted.GetSeconds () <<
			      " Last seen: " << tmp.m_lastSeen.GetSeconds () <<
			      " modifying Part to: " << low_part);

		poa_index.modify (parts[vec_pos],
				    ll::bind (&PartPoASeen::UpdatePart, ll::_1, low_part));

		parts_poa_seen_by_natural_order::iterator i_start, i_stop, i_curr = order_index.begin ();

		Ptr<const Face> face_1 = 0;
		Ptr<const Face> face_2 = 0;
		Address poa_1;
		Address poa_2;

		uint32_t part_1 = 0;
		uint32_t part_2 = 0;

//		NS_LOG_DEBUG ("Compressing resulting initial table: ");
//		PrintDebug (st);

		while (i_curr != order_index.end ())
		  {
		    i_start = i_curr;
		    if (i_start != order_index.end ())
		      {
			i_curr++;
			if (i_curr != order_index.end ())
			  {
			    i_stop = i_curr;
			  }
			else
			  {
			    break;
			  }
		      }
		    else
		      {
			break;
		      }

		    face_1 = i_start->m_face;
		    face_2 = i_stop->m_face;

		    poa_1 = i_start->m_poa;
		    poa_2 = i_stop->m_poa;

		    part_1 = i_start->m_part;
		    part_2 = i_stop->m_part;

		    //		    NS_LOG_DEBUG ("Verifying 3N name: " << *(name_1) << " Part: " << part_1 <<
		    //				  " vs 3N name: " << *(name_2) << " Part: " << part_2);

		    if (face_1->GetId () == face_2->GetId ())
		      {
			if (poa_1 == poa_2)
			  {
			    if (part_1 >= part_2)
			      {
				NS_LOG_DEBUG ("Eliminating 1st Face: " << face_1->GetId () <<
					      " PoA: " << poa_1 << " Part: " << part_1);
				i_curr = order_index.erase (i_start);
			      }
			    else
			      {
				NS_LOG_DEBUG ("Eliminating 2nd Face: " << face_2->GetId () <<
					      " PoA: " << poa_2 << " Part: " << part_2);
				i_curr = i_start;
				order_index.erase (i_stop);
			      }
			  }
			else
			  {
			    if (part_1 == part_2)
			      {
				NS_LOG_DEBUG ("Eliminating 1st Face: " << face_1->GetId () <<
					      " PoA: " << poa_1 << " Part: " << part_1 <<
					      " due to repeat part number");
				i_curr = order_index.erase (i_start);
			      }
			  }
		      }
		    else
		      {
			if (part_1 == part_2)
			  {
			    NS_LOG_DEBUG ("Eliminating 1st Face: " << face_1->GetId () <<
					  " PoA: " << poa_1 << " Part: " << part_1 <<
					  " due to repeat part number");
			    i_curr = order_index.erase (i_start);
			  }
		      }
		  }
	//		NS_LOG_DEBUG ("Compressed table: ");
	//		PrintDebug (st);
		return true;
	      }
	    else
	      {
		if ((min_pos > 0) && (low_part > max_max_part))
		  {
		    NS_LOG_DEBUG ("Due to entry position being at " << min_pos << " of " <<
				  total_parts <<  " and previous part being " <<
				  max_max_part << " (<) inserting new " <<
				  addr << " using Part: " << low_part);
		    m_src_poa_addrs.insert(PartPoASeen (face, addr, low_part));
		    return true;
		  }
		else
		  {
		    NS_LOG_DEBUG ("No modifications required to parts from update Face: " <<
				  face->GetId () << " PoA: " << addr << " part: " << low_part);
		    return false;
		  }
	      }
	  }
	else
	  {
	    NS_LOG_DEBUG ("No entry for Face " << face->GetId () << " PoA: " << addr <<
			  ", creating with " << low_part);
	    m_src_poa_addrs.insert(PartPoASeen (face, addr, low_part));
	    return true;
	  }
      }

      void
      Entry::UpdateStatus (Ptr<Face> face, FaceMetric::Status status)
      {
	NS_LOG_FUNCTION (this << boost::cref(*face) << status);

	NS_LOG_INFO ("Searching for Face " << boost::cref(*face));

	faces_by_ptr::iterator record = m_faces.get<i_face> ().find (face);
	if (record == m_faces.get<i_face> ().end ())
	  {
	    NS_LOG_INFO ("Face " << boost::cref(*face) << " not found, returning!");
	    return;
	  }

	NS_LOG_INFO ("Face " << boost::cref(*face) << " found, modifying status");
	m_faces.modify (record,
			ll::bind (&FaceMetric::SetStatus, ll::_1, status));

	NS_LOG_INFO ("Face " << boost::cref(*face) << " reordering random access");
	// reordering random access index same way as by metric index
	m_faces.get<i_nth> ().rearrange (m_faces.get<i_metric> ().begin ());
	NS_LOG_INFO ("Face " << boost::cref(*face) << " update complete");
      }

      void
      Entry::UpdateStatus (Ptr<Face> face, const Address& addr, FaceMetric::Status status)
      {
	NS_LOG_FUNCTION (this << boost::cref(*face) << " : " << addr << " : " << status);

	NS_LOG_INFO ("Searching for Face " << boost::cref(*face));

	faces_by_ptr::iterator record = m_faces.get<i_face> ().find (face);
	if (record == m_faces.get<i_face> ().end ())
	  {
	    NS_LOG_INFO ("Face " << boost::cref(*face) << " not found, returning!");
	    return;
	  }

	NS_LOG_INFO ("Face " << boost::cref(*face) << " found, modifying status");
	m_faces.modify (record,
			ll::bind (&FaceMetric::SetPoAStatus, ll::_1, addr, status));

	NS_LOG_INFO ("Face " << boost::cref(*face) << " reordering random access");
	// reordering random access index same way as by metric index
	m_faces.get<i_nth> ().rearrange (m_faces.get<i_metric> ().begin ());
	NS_LOG_INFO ("Face " << boost::cref(*face) << " update complete");
      }


      void
      Entry::AddOrUpdateRoutingMetric (Ptr<Face> face, int32_t metric)
      {
	NS_LOG_FUNCTION (this);
	NS_ASSERT_MSG (face != NULL, "Trying to Add or Update NULL face");

	faces_by_ptr::iterator record = m_faces.get<i_face> ().find (face);
	if (record == m_faces.get<i_face> ().end ())
	  {
	    NS_LOG_DEBUG ("No entry for Face " << face->GetId () << " inserting");
	    m_faces.insert (FaceMetric (face, metric));
	  }
	else
	  {
	    // don't update metric to higher value
	    if (record->GetRoutingCost () > metric || record->GetStatus () == FaceMetric::ICN_FIB_RED)
	      {
		m_faces.modify (record,
				ll::bind (&FaceMetric::SetRoutingCost, ll::_1, metric));

		m_faces.modify (record,
				ll::bind (&FaceMetric::SetStatus, ll::_1, FaceMetric::ICN_FIB_YELLOW));
	      }
	    else
	      {
		NS_LOG_DEBUG ("Not updating Face " << face->GetId () << " due to higher value " <<
			      record->GetRoutingCost () << " vs " << metric);
	      }
	  }

	// reordering random access index same way as by metric index
	m_faces.get<i_nth> ().rearrange (m_faces.get<i_metric> ().begin ());
      }

      void
      Entry::SetRealDelayToProducer (Ptr<Face> face, Time delay)
      {
	NS_LOG_FUNCTION (this);
	NS_ASSERT_MSG (face != NULL, "Trying to Update NULL face");

	faces_by_ptr::iterator record = m_faces.get<i_face> ().find (face);
	if (record != m_faces.get<i_face> ().end ())
	  {
	    m_faces.modify (record,
			    ll::bind (&FaceMetric::SetRealDelay, ll::_1, delay));
	  }
      }


      void
      Entry::Invalidate ()
      {
	for (faces_by_ptr::iterator face = m_faces.begin ();
	    face != m_faces.end ();
	    face++)
	  {
	    m_faces.modify (face,
			    ll::bind (&FaceMetric::SetRoutingCost, ll::_1, std::numeric_limits<uint16_t>::max ()));

	    m_faces.modify (face,
			    ll::bind (&FaceMetric::SetStatus, ll::_1, FaceMetric::ICN_FIB_RED));
	  }
      }

      const FaceMetric &
      Entry::FindBestCandidate (uint32_t skip/* = 0*/) const
      {
	if (m_faces.size () == 0) throw Entry::NoFaces ();
	skip = skip % m_faces.size();
	return m_faces.get<i_nth> () [skip];
      }

      Ptr<const NNNAddress>
      Entry::FindNewest3NDestination (uint32_t skip/* = 0*/)
      {
	NS_LOG_FUNCTION (this << skip);

	parts_seen_by_natural_order& seen_index = m_src_addrs.get<p_natural> ();
	parts_seen_by_natural_order::iterator it = seen_index.begin ();

	for (uint32_t i = 0; i < skip; i++)
	  {
	    ++it;
	  }

	if (it != seen_index.end ())
	  {
	    Ptr<const NNNAddress> addr = it->m_name;
	    NS_LOG_DEBUG ("Found 3N name for entry:" << *addr << " Part: " << it->m_part <<
			  " Inserted: " << it->m_firstInserted.GetSeconds () <<
			  " Last seen: " << it->m_lastSeen.GetSeconds ());
	    return addr;
	  }
	else
	  {
	    NS_LOG_DEBUG ("Found no 3N name for skip of " << skip);
	    return 0;
	  }
      }

      std::set<Ptr<const NNNAddress>, Ptr3NSizeMaxComp>
      Entry::Find3NDestinations (Time threshold)
      {
	NS_LOG_FUNCTION (this);
	std::set<Ptr<const NNNAddress>, Ptr3NSizeMaxComp> ans;

	parts_seen_by_natural_order& seen_index = m_src_addrs.get<p_natural> ();
	parts_seen_by_natural_order::iterator it = seen_index.begin ();

	while (it != seen_index.end ())
	  {
	    if ((Simulator::Now () - it->m_lastSeen) > threshold)
	      break;

	    ans.insert(it->m_name);
	    ++it;
	  }

	return ans;
      }

      PartsSeenContainer
      Entry::Find3NDestinationsByTime (Time threshold)
      {
	NS_LOG_FUNCTION (this << threshold.GetSeconds ());

	if (threshold.IsZero())
	  {
	    NS_LOG_DEBUG ("Received a 0 threshold");
	    return m_src_addrs;
	  }

	PartsSeenContainer ans;
	parts_seen_by_natural_order& seen_index = m_src_addrs.get<p_natural> ();
	parts_seen_by_natural_order::iterator it = seen_index.begin();

	while (it != seen_index.end ())
	  {
	    if ((Simulator::Now () - it->m_lastSeen) > threshold)
	      break;

	    ans.insert(*it);
	    ++it;
	  }

	return ans;
      }

      PartsPoASeenContainer
      Entry::FindPoADestinationsByTime (Time threshold)
      {
	NS_LOG_FUNCTION (this << threshold.GetSeconds ());

	if (threshold.IsZero())
	  {
	    NS_LOG_DEBUG ("Received a 0 threshold");
	    return m_src_poa_addrs;
	  }

	PartsPoASeenContainer ans;
	parts_poa_seen_by_natural_order& seen_index = m_src_poa_addrs.get<p_natural> ();
	parts_poa_seen_by_natural_order::iterator it = seen_index.begin();

	while (it != seen_index.end ())
	  {
	    if ((Simulator::Now () - it->m_lastSeen) > threshold)
	      break;

	    ans.insert(*it);
	    ++it;
	  }

	return ans;
      }

      size_t
      Entry::GetPartSrcEntries () const
      {
	return m_src_addrs.size();
      }

      size_t
      Entry::GetPartPoAEntries () const
      {
	return m_src_poa_addrs.size();
      }

      Ptr<Fib>
      Entry::GetFib ()
      {
	return m_fib;
      }

      std::ostream& operator<< (std::ostream& os, const Entry &entry)
      {
	faces_by_index::iterator metric = entry.m_faces.get<i_nth> ().begin ();

	while (metric != entry.m_faces.get<i_nth> ().end ())
	  {
	    os << boost::format(fib_no_formatter) % " " % *metric % "-" % (metric->m_firstSeen).GetSeconds ()  % "-" % (metric->m_lastSeen).GetSeconds ();
	    ++metric;
	  }

	if (entry.m_src_addrs.size () > 0)
	  {
	    os << "3N Sources for Parts size: " << entry.m_src_addrs.size () << std::endl;
	    os << "        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%" << std::endl;
	    os << boost::format(fib_formatter) % "" % "Auth 3N" % "" % "" % "" % "";
	    os << "        -----------------------------------------------------------------------" << std::endl;
	    parts_seen_by_natural_order::iterator seen_index1 = entry.m_src_addrs.get<p_natural> ().begin();

	    while (seen_index1 != entry.m_src_addrs.get<p_natural> ().end ())
	      {
		os << boost::format(fib_formatter) % "" %
		    *(seen_index1->m_name) %
		    seen_index1->m_part %
		    (seen_index1->m_firstInserted).GetSeconds () %
		    (seen_index1->m_partmod).GetSeconds () %
		    (seen_index1->m_lastSeen).GetSeconds();
		++seen_index1;
	      }
	  }

	if (entry.m_src_poa_addrs.size () > 0)
	  {
	    os << "PoA Sources for Parts size: " << entry.m_src_poa_addrs.size () << std::endl;
	    os << "  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%" << std::endl;
	    os << boost::format(fib_poa_title_formatter) % "" % "FC" % "PoA" % "Part" % "Inserted" % "Mod part" % "Last seen";
	    os << "-------------------------------------------------------------------------------" << std::endl;
	    parts_poa_seen_by_natural_order::iterator seen_index1 = entry.m_src_poa_addrs.get<p_natural> ().begin();

	    while (seen_index1 != entry.m_src_poa_addrs.get<p_natural> ().end ())
	      {
		os << boost::format(fib_poa_formatter) % "" %
		    seen_index1->m_face->GetId () %
		    seen_index1->m_poa %
		    seen_index1->m_part %
		    (seen_index1->m_firstInserted).GetSeconds () %
		    (seen_index1->m_partmod).GetSeconds () %
		    (seen_index1->m_lastSeen).GetSeconds()
		    ;

		++seen_index1;
	      }
	  }

	if (entry.m_dst_addrs.size () > 0)
	  {
	    os << "Destinations size: " << entry.m_dst_addrs.size () << std::endl;
	    os << "        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%" << std::endl;
	    os << boost::format(fib_formatter) % "" % "Possible 3N" % "" % "" % "" % "";
	    os << "        -----------------------------------------------------------------------" << std::endl;
	    parts_seen_by_natural_order::iterator seen_index2 = entry.m_dst_addrs.get<p_natural> ().begin();

	    while (seen_index2 != entry.m_dst_addrs.get<p_natural> ().end ())
	      {
		os << boost::format(fib_formatter) % "" % *(seen_index2->m_name) %
		    seen_index2->m_part % (seen_index2->m_firstInserted).GetSeconds() %
		    (seen_index2->m_partmod).GetSeconds () % (seen_index2->m_lastSeen).GetSeconds ();
		++seen_index2;
	      }
	  }
	return os;
      }

      std::ostream& operator<< (std::ostream& os, const FaceMetric &metric)
      {
	static const std::string statusString[] = {" ","G","Y","R"};

	os << boost::format(fibfm_formatter) % metric.m_face->GetId () % metric.GetRoutingCost() % statusString [metric.m_status] % metric.m_face->GetMetric ();
	return os;
      }
    } // namespace fib
  } // namespace nnn
} // namespace ns3
