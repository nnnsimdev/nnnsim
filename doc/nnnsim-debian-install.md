# Install nnnsim with ns-3

The installation procedure seen below has been tested with Debian Buster 10.4.

1. Install the following packages using apt-get

        # apt-get install libboost1.67-all-de python-pygraphviz python-pygccxml libxml2 libxml2-dev libgsl23 libgsl-dev sqlite3 libsqlite3-dev doxygen git build-essential pkg-config valgrind python-goocalendar libgcrypt20 libgcrypt20-dev libgsl-dev
    
    If you do not wish to use nnnsim, you can skip steps 3 and 4. 

1. Download the [ns-allinone-3.31.tar.bz2](https://www.nsnam.org/release/ns-allinone-3.31.tar.bz2)

        $ wget https://www.nsnam.org/release/ns-allinone-3.31.tar.bz2
        
    If located behind a proxy server, a .wgetrc might be required. A simple example file is below.
    
        https_proxy = http://proxyUsername:proxyPassword@proxy.server:proxy.port
        http_proxy = http://proxyUsername:proxyPassword@proxy.server:proxy.port
        ftp_proxy = http://proxyUsername:proxyPassword@proxy.server:proxy.port
        use_proxy = on

2. Decompress the file to the desired directory

        $ tar xjvf ns-allinone-3.31.tar.bz2

3. Clone the nnnsim repository within the ns-allinone-3.31/ns-3.31/src directory

        ns-allinone-3.31/ns-3.31/src $ git clone https://bitbucket.org/nnnsimdev/nnnsim.git
        
    If located behing a proxy server, git may require proxy configuration. This can be done with the following command below before doing git clone.
    
        $ git config --global http.proxy http://proxyUsername:proxyPassword@proxy.server:proxy.port

4. If you want to be able to use Point-To-Point NetDevices in your ns-3 scenarios, a patch is required.
    To apply the patch, you can use the following command line:

        ns-allinone-3.31/ns-3.31/src $ patch -p 0 < nnnsim/doc/patches/3n-icn-enabled-point-to-point.patch

5. Once patched, you can build ns-3 with nnnsim using the supplied build.py script in ns-allinone-3.31.
    nnnsim also includes examples which we highly recommend seeing before doing anything else.

    The following command line takes care of the configuration and compilation of ns-3

        ns-allinone-3.31 $ ./build.py --enable-examples
